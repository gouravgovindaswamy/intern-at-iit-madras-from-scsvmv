/*header files*/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
/*global variable*/
int diff[100][100],diffsqr[100][100],a[100][100],b[100][100],c[100][100],d[100][100],e[100][100],i,j,n;
int sqrtra(int diff[100][100],int diffsqr[100][100],int n);
int main()
{

	/*reading n*n value*/
	printf("enter the n*n value");
	scanf("%d",&n);
	/*generating a random value for "a" matrix*/
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			diff[i][j]=rand()%10;
			printf("diff%d%d:%d\n",i+1,j+1,diff[i][j]);
		}
	}
	/*generating a random value for "b" matrix*/
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			diffsqr[i][j]=rand()%100;
			printf("diffsqr%d%d:%d\n",i+1,j+1,diffsqr[i][j]);
		}
	}
	/*to find clock time for normal function*/
	clock_t start,end;
	double cpu_time_used;

	/*function call*/
	start=clock();
	sqrtra(diff,diffsqr,n);
	end=clock();
	cpu_time_used=((double)(end-start))/CLOCKS_PER_SEC;

	printf("normal addition took %lf seconds to execute\n",cpu_time_used);
return 0;
}
int sqrtra(int diff[100][100],int diffsqr[100][100],int n)
{
for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
c[i][j]=diffsqr[i][j]/diff[i][j];
d[i][j]=c[i][j]+diff[i][j];

e[i][j]=c[i][j]-diff[i][j];

a[i][j]=(d[i][j]+e[i][j])/2;
b[i][j]=(d[i][j]-e[i][j])/2;
}
}
/*d value display*/ 
	  printf("\na+b value of a matrix is:\n\n");
	  for(i=0;i<n;i++)
	  {
	  for(j=0;j<n;j++)
	  {
	  printf("%d\t",d[i][j]);
	  if(j==n-1)
	  {
	  printf("\n\n");
	  }
	  }
	  }
/*e value display*/ 
	  printf("\na-b value of a matrix is:\n\n");
	  for(i=0;i<n;i++)
	  {
	  for(j=0;j<n;j++)
	  {
	  printf("%d\t",e[i][j]);
	  if(j==n-1)
	  {
	  printf("\n\n");
	  }
	  }
	  }
/*a value display*/ 
	  printf("\na value of a matrix is:\n\n");
	  for(i=0;i<n;i++)
	  {
	  for(j=0;j<n;j++)
	  {
	  printf("%d\t",a[i][j]);
	  if(j==n-1)
	  {
	  printf("\n\n");
	  }
	  }
	  }
/*b value display*/ 
	  printf("\nb value of a matrix is:\n\n");
	  for(i=0;i<n;i++)
	  {
	  for(j=0;j<n;j++)
	  {
	  printf("%d\t",b[i][j]);
	  if(j==n-1)
	  {
	  printf("\n\n");
	  }
	  }
	  }

return 0;
}


