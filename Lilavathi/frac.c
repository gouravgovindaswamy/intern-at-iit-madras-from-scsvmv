/*header files*/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int x1[100][100],x2[100][100],x3[100][100],y4[100][100],y2[100][100],y3[100][100],div2[100][100],x5[100][100],y5[100][100],y6[100][100],x6[100][100],div3[100][100],i,j,k,y,n,x7[100][100],x8[100][100],y7[100][100],y8[100][100],x9[100][100],y9[100][100],x10[100][100],y10[100][100];
int normaladd(int x1[100][100],int y4[100][100],int x2[100][100],int y2[100][100],int n);
int normalsub(int x1[100][100],int y4[100][100],int x2[100][100],int y2[100][100],int n);
int normaldiv(int x1[100][100],int y4[100][100],int x2[100][100],int y2[100][100],int n);
int normalsqr(int x1[100][100],int y4[100][100],int n);
int normalcu(int x1[100][100],int y4[100][100],int n);


int main()
{
printf("enter the n*n value");
	scanf("%d",&n);
        /*generating a random value for "x1" matrix*/
        for(i=0;i<n;i++)
            {
		for(j=0;j<n;j++)
		{
                  x1[i][j]=rand()%10;
                  printf("x1%d%d:%d\n",i+1,j+1,x1[i][j]);
		}
            }
/*generating a random value for "x2" matrix*/
        for(i=0;i<n;i++)
            {
		for(j=0;j<n;j++)
		{
                  x2[i][j]=rand()%10;
                  printf("x2%d%d:%d\n",i+1,j+1,x2[i][j]);
		}
            }
/*generating a random value for "y1" matrix*/
        for(i=0;i<n;i++)
            {
		for(j=0;j<n;j++)
		{
                  y4[i][j]=rand()%10;
                  printf("y4%d%d:%d\n",i+1,j+1,y4[i][j]);
		}
            }
/*generating a random value for "y2" matrix*/
        for(i=0;i<n;i++)
            {
		for(j=0;j<n;j++)
		{
                  y2[i][j]=rand()%10;
                  printf("y2%d%d:%d\n",i+1,j+1,y2[i][j]);
		}
            }
/*to print the numerator and denominator which should be added*/
printf("1st numerator and denominator\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%d/%d\t",x1[i][j],y4[i][j]);
			if(j==n-1)
			{
				printf("\n\n");
			}
		}
	}
printf("2nd numerator and denominator\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%d/%d\t",x2[i][j],y2[i][j]);
			if(j==n-1)
			{
				printf("\n\n");
			}
		}
	}
 /*to find clock time for normal addition function*/
        clock_t start,end;
        double cpu_time;
        start=clock();
        /*function call*/
	normaladd(x1,y4,x2,y2,n);
        end=clock();
        cpu_time=((double)(end-start))/CLOCKS_PER_SEC;
        printf("normal fractional addition took %lf seconds to execute \n",cpu_time);
/*to find clock time for normal subtraction function*/
        clock_t st,en;
        double time;
        st=clock();
        /*function call*/
	normalsub(x1,y4,x2,y2,n);
        en=clock();
        time=((double)(en-st))/CLOCKS_PER_SEC;
        printf("normal fractional subtraction took %lf seconds to execute \n",time);
/*to find clock time for normal division function*/
        clock_t s,e;
        double t;
        s=clock();
        /*function call*/
	normaldiv(x1,y4,x2,y2,n);
        e=clock();
        t=((double)(e-s))/CLOCKS_PER_SEC;
        printf("normal fractional division took %lf seconds to execute \n",t);
/*to find clock time for normal square function*/
        clock_t s1,e1;
        double t1;
        s1=clock();
        /*function call*/
	normalsqr(x1,y4,n);
        e1=clock();
        t1=((double)(e1-s1))/CLOCKS_PER_SEC;
        printf("normal fractional square took %lf seconds to execute \n",t1);
/*to find clock time for normal cube function*/
        clock_t s2,e2;
        double t2;
        s2=clock();
        /*function call*/
	normalcu(x1,y4,n);
        e2=clock();
        t2=((double)(e2-s2))/CLOCKS_PER_SEC;
        printf("normal fractional cube took %lf seconds to execute \n",t2);

return 0;
}
int normaladd(int x1[100][100],int y4[100][100],int x2[100][100],int y2[100][100],int n)
{
for(i=0;i<n;i++)
{
for(j=0;j<n;j++)
{
/* calculating the numerator */
        x3[i][j] = (x1[i][j] * y2[i][j]) + (x2[i][j] * y4[i][j]);

        /* calculating the numerator */
        y3[i][j] = (y4[i][j] * y2[i][j]);
if (x3[i][j] > y3[i][j]) {
                div2[i][j] = y3[i][j];
        } 
else {
                div2[i][j] = x3[i][j];
        }
}
}
for(i=0;i<n;i++)
{
for(j=0;j<n;j++)
{
for (k= div2[i][j];k> 0; k--) {
                if (x3[i][j] % k == 0 && y3[i][j] % k == 0) {
                        x3[i][j] = x3[i][j] / k;
                        y3[i][j] = y3[i][j] / k;
                }
        }

      
}
}
printf("by ilavathi's fractional addition method\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%d/%d\t",x3[i][j],y3[i][j]);
			if(j==n-1)
			{
				printf("\n\n");
			}
		}
	}
}


int normalsub(int x1[100][100],int y4[100][100],int x2[100][100],int y2[100][100],int n)
{
for(i=0;i<n;i++)
{
for(j=0;j<n;j++)
{
/* calculating the numerator */
        x5[i][j] = (x1[i][j] * y2[i][j]) - (x2[i][j] * y4[i][j]);

        /* calculating the numerator */
        y5[i][j] = (y4[i][j] * y2[i][j]);
if (x5[i][j] > y5[i][j]) {
                div2[i][j] = y3[i][j];
        } 
else {
                div2[i][j] = x3[i][j];
        }
}
}
for(i=0;i<n;i++)
{
for(j=0;j<n;j++)
{
for (k= div2[i][j];k> 0; k--) {
                if (x5[i][j] % k == 0 && y5[i][j] % k == 0) {
                        x5[i][j] = x5[i][j] / k;
                        y5[i][j] = y5[i][j] / k;
                }
        }

      
}
}
printf("by ilavathi's fractional subtraction method\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%d/%d\t",x5[i][j],y5[i][j]);
			if(j==n-1)
			{
				printf("\n\n");
			}
		}
	}
}


int normaldiv(int x1[100][100],int y4[100][100],int x2[100][100],int y2[100][100],int n)
{
for(i=0;i<n;i++)
{
for(j=0;j<n;j++)
{
/* calculating the numerator */
        x6[i][j] = (x1[i][j] *  y2[i][j]); 

        /* calculating the numerator */
        y6[i][j] = (y4[i][j] * x2[i][j]);
if (x6[i][j] > y6[i][j]) {
                div3[i][j] = y6[i][j];
        } 
else {
                div3[i][j] = x6[i][j];
        }
}
}
for(i=0;i<n;i++)
{
for(j=0;j<n;j++)
{
for (k= div3[i][j];k> 0; k--) {
                if (x6[i][j] % k == 0 && y6[i][j] % k == 0) {
                        x6[i][j] = x6[i][j] / k;
                        y6[i][j] = y6[i][j] / k;
                }
        }

      
}
}
printf("by ilavathi's fractinal division method\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%d/%d\t",x6[i][j],y6[i][j]);
			if(j==n-1)
			{
				printf("\n\n");
			}
		}
	}
}
int normalsqr(int x1[100][100],int y4[100][100],int n)
{
for(i=0;i<n;i++)
{
for(j=0;j<n;j++)
{
/* calculating the numerator */
        x7[i][j] = (x1[i][j] *  x1[i][j]); 

        /* calculating the numerator */
        y7[i][j] = (y4[i][j] * y4[i][j]);
/*try*/


      
}
}
printf("by ilavathi's square division method\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%d/%d\t",x7[i][j],y7[i][j]);
			if(j==n-1)
			{
				printf("\n\n");
			}
		}
	}
}


int normalcu(int x1[100][100],int y4[100][100],int n)
{
for(i=0;i<n;i++)
{
for(j=0;j<n;j++)
{
/* calculating the numerator */
        x8[i][j] = (x1[i][j] *  x1[i][j] *x1[i][j]); 

        /* calculating the numerator */
        y8[i][j] = (y4[i][j] * y4[i][j]*y4[i][j]);

      
}
}
printf("by ilavathi's cube division method\n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%d/%d\t",x8[i][j],y8[i][j]);
			if(j==n-1)
			{
				printf("\n\n");
			}
		}
	}
   
}





